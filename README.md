## Create input vectors

### interests list

Code để chuyển phần interest_list sang one-hot trong file `vectorize.py` nhé. Ý tưởng chính là loop qua một lượt các sample, đếm xem có bao nhiêu interest id (36 cái). Sau đó tạo sẵn 1 mảng numpy (N, 36), rồi loop lại qua các sample để set giá trị của vector.  

```
python vectorize.py 
```

Nếu em chưa biết về set, dict với list khác nhau như thế nào thì đọc thêm ở đây xem nhé : 

https://docs.python.org/3/tutorial/datastructures.html

https://python.swaroopch.com/data_structures.html

### weekly distance (km)

Đối với weekly distance thì nhìn bảng summary ở dưới này thì < 8km chắc chắn là nữ :D , > 3200km chắc chắn là nam. 

Bình thường với giá trị kiểu này thì vectorize bằng binning ? 

![Weekly distance summary](images/weekly_distance_km.png "Weekly Distance Summary") 


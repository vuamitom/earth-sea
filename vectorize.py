import pandas as pd 
import numpy as np

with open('./model_data.csv', 'rb') as f:
    model_data = pd.read_csv(f, delimiter='|')

print('OVERALL STATS:')
print(model_data.describe())
print('MALE STATS:')
print(model_data.loc[model_data['gender'] == 'male'].describe())
print('FEMALE STATS:')
print(model_data.loc[model_data['gender'] == 'female'].describe())

# get all interests
# TODO: read about data structure - set, dict, list ...
unique_interests = set()
for val in model_data['interest_list']:
    if isinstance(val, str):
        interests = val.split(',')
        for x in interests:
            unique_interests.add(x)


# convert a set to a list
unique_interests = list(unique_interests)

# create a empty numpy array  for input vector
# no. of rows = no. of samples
# no. of columns = no of. interests
num_samples = model_data.count()['row_num']
inputs = np.zeros(shape=(num_samples, len(unique_interests)))
labels = np.zeros(shape=(num_samples, 1))

# create embeddings
# iterate through rows of data frame
for index, row in model_data.iterrows():
    if isinstance(row['interest_list'], str):
        interests = row['interest_list'].split(',')
        for x in interests:
            idx = unique_interests.index(x)
            inputs[index][idx] = 1
        assert(sum(inputs[index]) == len(interests))
    labels[index] = 1 if row['interest_list'] == 'male' else 0

# input vectors 
print(inputs)



